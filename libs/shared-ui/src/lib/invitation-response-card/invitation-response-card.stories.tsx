import type { Meta, StoryObj } from '@storybook/react';
import { InvitationResponseCard } from './invitation-response-card';
import { InvitationStatus } from 'core';
import { invitation } from '../utils';
import { within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';

const meta: Meta<typeof InvitationResponseCard> = {
  component: InvitationResponseCard,
  title: 'Client/Invitation Response Card',
};
export default meta;
type Story = StoryObj<typeof InvitationResponseCard>;

export const Accepted: Story = {
  args: {
    invitation,
    acceptInvitation: async () => {},
    declineInvitation: async () => {},
  },
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/Invitation accepted/gi)).toBeTruthy();
    expect(canvas.getByText(/2 people joining/gi)).toBeTruthy();
  },
};

export const Declined: Story = {
  args: {
    ...Accepted.args,
    invitation: {
      ...invitation,
      status: InvitationStatus.Declined,
    },
  },
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/Invitation declined/gi)).toBeTruthy();
  },
};

export const Pending: Story = {
  args: {
    ...Accepted.args,
    invitation: {
      ...invitation,
      status: InvitationStatus.Pending,
    },
  },
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/Let us know how many will join/gi)).toBeTruthy();
    expect(
      canvas.getByText(/or whether you are unable to participate/gi)
    ).toBeTruthy();
  },
};
