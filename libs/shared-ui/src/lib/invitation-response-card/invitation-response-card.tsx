import { Invitation, InvitationStatus, I18n } from 'core';
import InvitationAcceptanceForm from '../invitation-acceptance-form/invitation-acceptance-form';
import InvitationDeclinationForm from '../invitation-declination-form/invitation-declination-form';

export interface InvitationResponseCardProps extends I18n {
  invitation: Invitation;
  acceptInvitation: (uuid: string, formData: FormData) => Promise<void>;
  declineInvitation: (uuid: string) => Promise<void>;
}

export function InvitationResponseCard({
  invitation,
  acceptInvitation,
  declineInvitation,
  t,
  locale,
}: InvitationResponseCardProps) {
  const { status, acceptedCount } = invitation;

  return (
    <div className="font-bellefair">
      {/* Accepted */}
      {status === InvitationStatus.Accepted && (
        <div className="flex flex-col items-center text-lime-green-900 p-3 rounded bg-lime-green-300 bg-opacity-25 sm:py-4 sm:px-6">
          <div className="text-lg font-semibold">{t('respose:accepted')}</div>
          <div className="text-sm">
            {t('respose:acceptedGuestsCount', { count: String(acceptedCount) })}
          </div>
        </div>
      )}

      {/* Declined */}
      {status === InvitationStatus.Declined && (
        <div className="text-center text-lg text-red-900 font-semibold p-3 rounded bg-red-300 bg-opacity-25 sm:py-4 sm:px-6">
          {t('respose:declined')}
        </div>
      )}

      {/* Pending */}
      {status === InvitationStatus.Pending && (
        <div className="flex flex-col space-y-4 mt-6 p-4 border-t border-b border-grey-300 border-dotted">
          <p className="text-grey-500">{t('form:textAccept')}</p>

          <InvitationAcceptanceForm
            invitation={invitation}
            acceptInvitation={acceptInvitation}
            t={t}
            locale={locale}
          />

          <p className="text-grey-500 pt-4">{t('form:textDecline')}</p>

          <div className="self-center">
            <InvitationDeclinationForm
              invitation={invitation}
              declineInvitation={declineInvitation}
              t={t}
              locale={locale}
            />
          </div>
        </div>
      )}
    </div>
  );
}

export default InvitationResponseCard;
