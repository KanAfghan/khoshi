import { Invitation, InvitationStatus } from 'core';

export const invitation: Invitation = {
  uuid: '00000000-0000-0000-0000-000000000000',
  msg: 'Partner',
  invitedCount: 2,
  idx: 1,
  status: InvitationStatus.Accepted,
  acceptedCount: 2,
};
