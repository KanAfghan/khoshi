export function Component() {
  return <h1>Welcome to HelloServer!</h1>;
}

export async function HelloServer() {
  return <Component />;
}
