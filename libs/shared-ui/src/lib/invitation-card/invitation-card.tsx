import { I18n } from 'core';

interface I18nName {
  latin: string;
  native?: string;
}

export interface InvitationCardProps extends I18n {
  imageUrl: string;
  groomName: I18nName;
  brideName: I18nName;
  msg?: string;
  pendingResponse?: boolean;
  accepted?: boolean;
}

export function InvitationCard({
  imageUrl,
  groomName,
  brideName,
  msg,
  pendingResponse,
  accepted,
  t,
  locale,
}: InvitationCardProps) {
  const locationNameEnvVar = `NEXT_PUBLIC_EVENT_LOCATION_NAME_${locale?.toUpperCase()}`;

  return (
    <div
      className={
        `flex-1 flex flex-col space-y-6` +
        (locale !== 'en' ? ' font-glyph' : '')
      }
    >
      <div className="w-[45vw] md:w-[40vw] lg:w-[30vw] xl:w-[20vw] 2xl:w-[15vw] mx-auto my-4">
        <img
          src={imageUrl}
          alt="the groom and bride"
          className="w-full h-auto opacity-80"
        />
      </div>

      <div
        className={
          'text-center text-grey-900 p-3 mb-2 sm:py-4 sm:px-6 text-3xl'
        }
      >
        <div className="font-belleza font-light tracking-widest uppercase">
          {groomName.latin}
          <img
            src="/icons/amb.svg"
            alt="ambersand"
            className="inline-block mx-2 -mb-3"
          />
          {brideName.latin}
        </div>

        {locale !== 'en' && (
          <div className="font-mono text-2xl font-bold mt-3">
            {t('invitation:names', {
              groom_name: groomName.native as string,
              bride_name: brideName.native as string,
            })}
          </div>
        )}
      </div>

      <div
        className={
          'block text-center uppercase text-grey-900 max-w-md mx-auto ' +
          (locale !== 'en'
            ? 'font-glyph text-lg'
            : 'font-bellefair tracking-widest')
        }
      >
        {t('invitation:text', {
          msg: (msg?.length ?? 0) > 0 ? (msg as string) : 'your',
        })}
      </div>

      <div className="w-[20vw] mx-auto lg:w-40">
        <img
          src="/images/flower-sketch.svg"
          alt="flower sketch"
          className="w-full h-auto"
        />
      </div>

      <div className="font-bellefair flex flex-col justify-center items-center text-grey-900">
        <div className="flex flex-col space-y-1 justify-center items-center">
          <h2
            className={
              'text-xl leading-none ' +
              (locale === 'en' ? 'tracking-widest' : '')
            }
          >
            {t('invitation:eventDay')}
          </h2>
          <div className="text-[#bd1818] flex flex-row space-x-3 items-center rtl:space-x-reverse">
            <span>{t('invitation:eventMonth')}</span>
            <div className="border-l border-[#f8f2e7] border-6 border-dotted w-1 h-6" />
            <span className="text-2xl">{t('invitation:eventDate')}</span>
            <div className="border-l border-[#f8f2e7] border-6 border-dotted w-1 h-6" />
            <span>{t('invitation:eventYear')}</span>
          </div>
          <p className="text-sm border-t border-b border-dotted border-6 border-[#f8f2e7]">
            {t('invitation:eventTime')}
          </p>
        </div>

        <div className="mt-6">
          <p className="text-center">
            {process.env[locationNameEnvVar] ??
              process.env.NEXT_PUBLIC_EVENT_LOCATION_NAME}
          </p>
          <div className="flex flex-row justify-center rtl:flex-row-reverse">
            <div className="text-grey-900">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 50 50"
                enableBackground="new 0 0 50 50"
                className="w-6 h-6"
              >
                <path d="M25 42.5l-.8-.9C23.7 41.1 12 27.3 12 19c0-7.2 5.8-13 13-13s13 5.8 13 13c0 8.3-11.7 22.1-12.2 22.7l-.8.8zM25 8c-6.1 0-11 4.9-11 11 0 6.4 8.4 17.2 11 20.4 2.6-3.2 11-14 11-20.4 0-6.1-4.9-11-11-11z" />
                <path d="M25 24c-2.8 0-5-2.2-5-5s2.2-5 5-5 5 2.2 5 5-2.2 5-5 5zm0-8c-1.7 0-3 1.3-3 3s1.3 3 3 3 3-1.3 3-3-1.3-3-3-3z" />
              </svg>
            </div>
            <a
              href={process.env.NEXT_PUBLIC_EVENT_LOCATION_MAP_URL}
              target="_blank"
              rel="noreferrer"
              className="flex flex-col items-center hover:text-lime-green-600 active:text-lime-green-600"
            >
              {(process.env.NEXT_PUBLIC_EVENT_LOCATION_ADDRESS ?? '')
                .split('<br>')
                .map((line, i) => (
                  <span key={i}>{line}</span>
                ))}
            </a>
          </div>
        </div>
        {pendingResponse && (
          <p className="mt-6">
            {t('invitation:rsvp')}{' '}
            <span className="text-sm">{t('invitation:rsvpBy')}</span>
          </p>
        )}
        {accepted && locale === 'en' && (
          <p className="mt-6 leading-tight">
            NOTE:{' '}
            <span className="text-sm">
              We kindly request that you consider making a donation to a
              charitable organization on our behalf instead of giving us a
              wedding present/gift.
            </span>
          </p>
        )}
      </div>
    </div>
  );
}

export default InvitationCard;
