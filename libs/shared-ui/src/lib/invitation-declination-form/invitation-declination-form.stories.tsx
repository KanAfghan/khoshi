import type { Meta, StoryObj } from '@storybook/react';
import { InvitationDeclinationForm } from './invitation-declination-form';

import { within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';
import { invitation } from '../utils';

const meta: Meta<typeof InvitationDeclinationForm> = {
  component: InvitationDeclinationForm,
  title: 'Client/InvitationDeclinationForm',
};
export default meta;
type Story = StoryObj<typeof InvitationDeclinationForm>;

export const Default: Story = {
  args: {
    invitation,
    declineInvitation: async () => {},
  },
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);
    expect(canvas.getByText(/We can't participate/gi)).toBeTruthy();
  },
};
