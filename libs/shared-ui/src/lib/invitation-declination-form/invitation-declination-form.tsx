import { I18n, Invitation } from 'core';
import SubmitButton from '../submit-button/submit-button';

export interface InvitationDeclinationFormProps extends I18n {
  invitation: Invitation;
  declineInvitation: (uuid: string) => Promise<void>;
}

export function InvitationDeclinationForm({
  invitation,
  declineInvitation,
  t,
}: InvitationDeclinationFormProps) {
  const { uuid } = invitation;

  const declineInvitationAction = declineInvitation.bind(null, uuid);

  return (
    <form action={declineInvitationAction}>
      <SubmitButton variant="danger" pendingText={t('form:declineBtnPending')}>
        {t('form:declineBtn')}
      </SubmitButton>
    </form>
  );
}

export default InvitationDeclinationForm;
