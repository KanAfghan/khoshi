import { PropsWithChildren, ButtonHTMLAttributes } from 'react';

export interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  variant?: 'primary' | 'danger';
}

export function Button({
  variant = 'primary',
  children,
}: PropsWithChildren<ButtonProps>) {
  const baseCls = 'px-6 py-1.5 rounded-md font-semibold shadow-md';
  const primaryCls = ' border-grey-300 border text-lime-green-900';
  const dangerCls = '  border-grey-300 border text-red-900';

  return (
    <button
      className={
        baseCls +
        (variant === 'primary' ? primaryCls : '') +
        (variant === 'danger' ? dangerCls : '')
      }
    >
      {children}
    </button>
  );
}

export default Button;
