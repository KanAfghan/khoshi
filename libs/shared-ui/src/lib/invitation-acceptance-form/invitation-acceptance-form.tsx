import { I18n, Invitation } from 'core';
import SubmitButton from '../submit-button/submit-button';

export interface InvitationAcceptanceFormProps extends I18n {
  invitation: Invitation;
  acceptInvitation: (uuid: string, formData: FormData) => Promise<void>;
}

export function InvitationAcceptanceForm({
  invitation,
  acceptInvitation,
  t,
}: InvitationAcceptanceFormProps) {
  const { uuid, invitedCount } = invitation;

  const acceptInvitationAction = acceptInvitation.bind(null, uuid);

  return (
    <form
      action={acceptInvitationAction}
      className="flex justify-center items-center"
    >
      <div className="flex space-x-6 items-end">
        <label htmlFor="count" className="text-grey-700 pb-2">
          {t('form:enterGuests')}:
        </label>
        <input
          className="text-center border-b border-b-grey-300 font-semibold text-lime-green-900 py-1.5 w-10"
          type="number"
          name="count"
          min={0}
          max={invitedCount}
          defaultValue={invitedCount}
        />
        <SubmitButton pendingText={t('form:acceptBtnPending')}>
          {t('form:acceptBtn')}
        </SubmitButton>
      </div>
    </form>
  );
}

export default InvitationAcceptanceForm;
