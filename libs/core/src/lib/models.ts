export type DbCol =
  | 0
  | 1
  | 2
  | 3
  | 4
  | 5
  | 6
  | 7
  | 8
  | 9
  | 10
  | 11
  | 12
  | 13
  | 14
  | 15;

export enum InvitationStatus {
  Accepted = 'accepted',
  Declined = 'declined',
  Pending = 'pending',
}

export interface Invitation {
  uuid: string;
  idx: number;
  msg?: string;
  invitedCount: number;
  acceptedCount?: number;
  status: InvitationStatus;
  link?: string;
  guest?: Guest;
  viewsCount?: number;
  groomName?: string;
  brideName?: string;
  sentAt?: Date;
  sid?: string;
  locale?: Locale;
  errors?: string;
}

export interface Guest {
  name: string;
  email?: string;
  phone: string;
  isTester?: boolean;
}

export type Translator = (
  key: string,
  params?: Record<string, string>
) => string;

export type Locale = 'en' | 'fa' | 'ps';

export interface I18n {
  t: Translator;
  locale?: Locale;
}
