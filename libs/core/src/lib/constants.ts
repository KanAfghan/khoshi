import { Locale } from './models';

export const DB_UUID_COLUMN = 0;
export const DB_NAME_COLUMN = 1;
export const DB_INVITED_COUNT_COLUMN = 2;
export const DB_MSG_COLUMN = 3;
export const DB_LANGUAGE_COLUMN = 4;
export const DB_EMAIL_COLUMN = 5;
export const DB_PHONE_COLUMN = 6;
export const DB_LINK_COLUMN = 7;
export const DB_IS_TESTER_COLUMN = 8;
export const DB_ACCEPTED_COUNT_COLUMN = 9;
export const DB_VIEWS_COUNT_COLUMN = 10;
export const DB_GROOM_NAME_COLUMN = 11;
export const DB_BRIDE_NAME_COLUMN = 12;
export const DB_SENT_AT_COLUMN = 13;
export const DB_SID_COLUMN = 14;
export const DB_ERRORS_COLUMN = 15;

export const DB_A1NOTATION = {
  [DB_UUID_COLUMN]: 'A',
  [DB_NAME_COLUMN]: 'B',
  [DB_INVITED_COUNT_COLUMN]: 'C',
  [DB_MSG_COLUMN]: 'D',
  [DB_LANGUAGE_COLUMN]: 'E',
  [DB_EMAIL_COLUMN]: 'F',
  [DB_PHONE_COLUMN]: 'G',
  [DB_LINK_COLUMN]: 'H',
  [DB_IS_TESTER_COLUMN]: 'I',
  [DB_ACCEPTED_COUNT_COLUMN]: 'J',
  [DB_VIEWS_COUNT_COLUMN]: 'K',
  [DB_GROOM_NAME_COLUMN]: 'L',
  [DB_BRIDE_NAME_COLUMN]: 'M',
  [DB_SENT_AT_COLUMN]: 'N',
  [DB_SID_COLUMN]: 'O',
  [DB_ERRORS_COLUMN]: 'P',
};

export const LANG_LOCALE_MAP: Record<string, Locale> = {
  Engelsk: 'en',
  Dari: 'fa',
  Pashto: 'ps',
};

export const LOCALE_LANG_MAP: Record<Locale, string> = {
  en: 'Engelsk',
  fa: 'Dari',
  ps: 'Pashto',
};

export const INVITATION_SMS_TEXT_PLACEHOLDERS = {
  name: '{{name}}',
  groomName: '{{groomName}}',
  brideName: '{{brideName}}',
  msg: '{{msg}}',
  link: '{{link}}',
};
