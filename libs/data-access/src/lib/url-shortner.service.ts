import { Redis, RedisConfigNodejs } from '@upstash/redis';
import { createHash } from 'node:crypto';

export class UrlShortnerService {
  constructor(private redisClient: Redis) {}

  async shorten(url: string): Promise<string> {
    try {
      const hash = this.createHash(url);

      this.log('Shortening URL:', JSON.stringify({ url, hash }));

      const res = await this.redisClient.set(hash, url);
      if (res) {
        return this.buildUrl(hash);
      }

      throw new Error('Redis set failed: ' + (res ?? ''));
    } catch (error) {
      this.log('[ERROR] Failed to shorten URL:', error);
    }

    return url;
  }

  async expand(hash: string): Promise<string | undefined> {
    try {
      const res = await this.redisClient.get(hash);
      if (res) {
        return res as string;
      }

      throw new Error('Redis get failed: ' + (res ?? ''));
    } catch (error) {
      this.log('[ERROR] Failed to expand URL:', error);
    }

    return undefined;
  }

  protected createHash(url: string): string {
    const hash = createHash('sha256');
    hash.update(url);
    return hash.digest('hex').slice(0, 8);
  }

  private buildUrl(hash: string): string {
    return `${process.env['APP_URL']}/s/${hash}`;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private log(...args: any[]) {
    console.log('[UrlShortnerService]', ...args);
  }
}

export const urlShortnerService = new UrlShortnerService(
  new Redis({
    url: process.env['UPSTASH_REDIS_REST_URL'],
    token: process.env['UPSTASH_REDIS_REST_TOKEN'],
  } as RedisConfigNodejs)
);
