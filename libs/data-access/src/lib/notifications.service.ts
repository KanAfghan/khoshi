import twilio, { Twilio } from 'twilio';
import { INVITATION_SMS_TEXT_PLACEHOLDERS, Invitation } from 'core';
import { UrlShortnerService, urlShortnerService } from './url-shortner.service';

const PHONE_NUMBERS_SEPARATOR = ';';
const PHONE_NUMBER_REGEX = /^\+?\d{6,14}$/;

export enum NotificationStrategy {
  SMS = 'SMS',
  WhatsApp = 'WhatsApp',
}

export class NotificationsService {
  private dryRun = false;

  private notificationStrategy: NotificationStrategy = NotificationStrategy.SMS;

  constructor(
    private client: Twilio,
    private urlShortner: UrlShortnerService
  ) {}

  set isDryRun(dryRun: boolean) {
    this.dryRun = dryRun;
  }

  set strategy(strategy: NotificationStrategy) {
    this.notificationStrategy = strategy;
  }

  async sendInvitationNotification(
    invitation: Invitation
  ): Promise<Invitation | null> {
    if (!invitation.guest?.phone?.length) return null;

    let phones: string[] = [];
    try {
      phones = this.parsePhoneNumbers(invitation.guest.phone);
      if (!phones.length) return null;

      const body = await this.createMessageBody(invitation);

      const sids = await Promise.all(
        phones.map(async (phone) => {
          const whatsappPhone = `whatsapp:${phone}`;

          const phoneNumber =
            this.notificationStrategy === NotificationStrategy.WhatsApp
              ? whatsappPhone
              : phone;

          return await this.sendMessage(phoneNumber, body);
        })
      );

      this.log(
        'Sent message to:',
        phones.join(', '),
        'with SIDs:',
        JSON.stringify(sids)
      );

      return sids.filter(Boolean).reduce((invitation, rep) => {
        const { sid, error } = rep ?? {};

        if (!sid && !error) {
          invitation.errors =
            (invitation.errors
              ? `${invitation.errors}${PHONE_NUMBERS_SEPARATOR}`
              : '') +
            'Failed to send ' +
            this.notificationStrategy;

          return invitation;
        }

        if (sid?.length) {
          invitation.sid =
            (invitation.sid
              ? `${invitation.sid}${PHONE_NUMBERS_SEPARATOR}`
              : '') + sid;
        }

        if (error?.length) {
          invitation.errors =
            (invitation.errors
              ? `${invitation.errors}${PHONE_NUMBERS_SEPARATOR}`
              : '') + error;
        }

        return invitation;
      }, invitation);
    } catch (error) {
      this.log(
        '[ERROR] Failed to send notifications to:',
        phones.join(', '),
        'due to:',
        error
      );
    }

    return null;
  }

  async fetchSentAt(phoneNumber: string): Promise<Date | undefined> {
    this.log('Checking if message has been delivered to:', phoneNumber);

    if (!phoneNumber?.length) return;

    let phones: string[] = [];
    try {
      phones = this.parsePhoneNumbers(phoneNumber);
      if (!phones.length) return;

      const sentAts = await Promise.all(
        phones.map(async (phoneNumber) => {
          if (this.dryRun) {
            this.log(
              '[DRY RUN] Checking if message has been delivered to:',
              phoneNumber
            );

            return new Date();
          }

          const msgs = await this.client.messages.list({
            to:
              this.notificationStrategy === NotificationStrategy.WhatsApp
                ? `whatsapp:${phoneNumber}`
                : phoneNumber,
          });
          if (msgs.length === 0) return;

          const msg = msgs.find(({ status }) =>
            ['delivered', 'read'].includes(status)
          );
          if (!msg) return;

          return msg.dateSent;
        })
      );

      return sentAts.find((sentAt) => !!sentAt);
    } catch (error) {
      this.log(
        '[ERROR] Failed to check if message has been delivered to:',
        phones.join(', '),
        'due to:',
        error
      );
    }

    return;
  }

  async fetchFailureReason(
    invitation: Invitation
  ): Promise<string | undefined> {
    this.log('Checking failure reason for:', JSON.stringify(invitation));

    try {
      if (!invitation.sid?.length)
        throw new Error(`Invitation ${invitation.uuid} has no SID!`);

      const reasons = await Promise.all(
        invitation.sid.split(PHONE_NUMBERS_SEPARATOR).map(async (sid) => {
          if (this.dryRun) {
            this.log('[DRY RUN] Checking failure reason for:', sid);

            return 'dry-run-reason';
          }

          const msg = await this.client.messages.get(sid as string).fetch();
          if (!msg)
            throw new Error(`Failed to fetch message instance by SID: ${sid}`);

          return [msg.status, msg.errorCode, msg.errorMessage].join('|');
        })
      );

      return reasons.filter(Boolean).join(PHONE_NUMBERS_SEPARATOR);
    } catch (error) {
      this.log('[fetchFailureReason()] [ERROR]', error);
    }

    return;
  }

  async didSendInvitationNotification(
    invitation: Invitation
  ): Promise<Invitation> {
    this.log('Checking invitation notification status');

    if (!invitation.guest?.phone?.length) return invitation;

    let phones: string[] = [];
    try {
      phones = this.parsePhoneNumbers(invitation.guest.phone);
      if (!phones.length) return invitation;

      const sids = await Promise.all(
        phones.map(async (phone) => {
          if (this.dryRun) {
            this.log('[DRY RUN] Checking SMS status for:', phone);

            return 'dry-run-sid';
          }

          const msgs = await this.client.messages.list({
            to:
              this.notificationStrategy === NotificationStrategy.WhatsApp
                ? `whatsapp:${phone}`
                : phone,
          });
          if (msgs.length === 0) return;

          const sids = msgs.map(({ sid }) => sid);

          return sids.join('|');
        })
      );

      this.log('Invitation notification status:', JSON.stringify(sids));

      invitation.sid = sids.filter(Boolean).join(PHONE_NUMBERS_SEPARATOR);

      return invitation;
    } catch (error) {
      this.log(
        '[ERROR] Failed to check invitation notification status for',
        phones.join(', '),
        'due to:',
        error
      );
    }

    return invitation;
  }

  private async createMessageBody(invitation: Invitation): Promise<string> {
    const { guest, msg, groomName, brideName, link, locale } = invitation;

    const shortLink = await this.urlShortner.shorten(link as string);

    const textEnvVar = `INVITATION_SMS_TEXT${
      locale !== 'en' ? '_' + locale?.toUpperCase() : ''
    }`;
    const groomNameEnvVar = `NEXT_PUBLIC_GROOM_NAME${
      locale !== 'en' ? '_' + locale?.toUpperCase() : ''
    }`;
    const brideNameEnvVar = `NEXT_PUBLIC_BRIDE_NAME${
      locale !== 'en' ? '_' + locale?.toUpperCase() : ''
    }`;

    const groomNameFromEnv = process.env[groomNameEnvVar] ?? '';
    const brideNameFromEnv = process.env[brideNameEnvVar] ?? '';

    return (process.env[textEnvVar] ?? '')
      .replace(INVITATION_SMS_TEXT_PLACEHOLDERS.name, guest?.name ?? '')
      .replace(
        INVITATION_SMS_TEXT_PLACEHOLDERS.groomName,
        locale !== 'en'
          ? groomNameFromEnv
          : groomName
          ? groomName
          : groomNameFromEnv
      )
      .replace(
        INVITATION_SMS_TEXT_PLACEHOLDERS.brideName,
        locale !== 'en'
          ? brideNameFromEnv ?? ''
          : brideName
          ? brideName
          : brideNameFromEnv
      )
      .replace(INVITATION_SMS_TEXT_PLACEHOLDERS.msg, msg?.length ? msg : 'you')
      .replace(INVITATION_SMS_TEXT_PLACEHOLDERS.link, shortLink ?? '');
  }

  private async sendMessage(
    phoneNumber: string,
    body: string
  ): Promise<{ sid: string; error?: string } | null> {
    try {
      if (this.dryRun) {
        this.log(
          '[DRY RUN] Sending SMS:',
          JSON.stringify({ body, phoneNumber })
        );

        return { sid: 'dry-run-sid', error: 'dry-run-error' };
      }

      const msg = await this.client.messages.create({
        body,
        to: phoneNumber,
        from:
          this.strategy === NotificationStrategy.WhatsApp
            ? process.env['TWILIO_FROM_NUMBER']
            : `whatsapp:${process.env['TWILIO_FROM_NUMBER']}`,
      });

      return { sid: msg.sid, error: msg.errorMessage };
    } catch (error) {
      this.log('[ERROR] Failed to send message via Twilio due to:', error);
    }

    return null;
  }

  private isValidPhoneNumber(phone: string): boolean {
    return PHONE_NUMBER_REGEX.test(phone);
  }

  private parsePhoneNumbers(phone: string): string[] {
    return phone
      .split(PHONE_NUMBERS_SEPARATOR)
      .map((phone) => phone.trim().replace(/('| )*/, ''))
      .filter((phone) => this.isValidPhoneNumber(phone))
      .map((phone) => (!(phone ?? '').startsWith('+') ? `+45${phone}` : phone));
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private log(...args: any[]): void {
    console.log('[NotificationsService] ', ...args);
  }
}

export const notificationsService = new NotificationsService(
  twilio(process.env['TWILIO_ACCOUNT_SID'], process.env['TWILIO_AUTH_TOKEN']),
  urlShortnerService
);
