import { sheets_v4, google } from 'googleapis';
import { SupabaseClient, createClient } from '@supabase/supabase-js';

import {
  DbCol,
  Invitation,
  InvitationStatus,
  DB_A1NOTATION,
  DB_ACCEPTED_COUNT_COLUMN,
  DB_EMAIL_COLUMN,
  DB_INVITED_COUNT_COLUMN,
  DB_IS_TESTER_COLUMN,
  DB_LINK_COLUMN,
  DB_MSG_COLUMN,
  DB_NAME_COLUMN,
  DB_PHONE_COLUMN,
  DB_UUID_COLUMN,
  DB_VIEWS_COUNT_COLUMN,
  DB_GROOM_NAME_COLUMN,
  DB_LANGUAGE_COLUMN,
  LANG_LOCALE_MAP,
  DB_BRIDE_NAME_COLUMN,
  DB_SENT_AT_COLUMN,
  DB_SID_COLUMN,
  DB_ERRORS_COLUMN,
} from 'core';

const UUID_REGEX =
  /^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;

export class DatabaseService {
  private dryRun: boolean = false;

  constructor(
    private sheets: sheets_v4.Sheets,
    private spreadsheetId: string,
    private sheetRange: string,
    private supbase: SupabaseClient,
    private supabaseTable: string
  ) {}

  set isDryRun(dryRun: boolean) {
    this.dryRun = dryRun;
  }

  async fetchAllInvitations(): Promise<Invitation[]> {
    try {
      const { data, error } = await this.supbase
        .from(this.supabaseTable)
        .select('*');

      if (error) {
        throw new Error(
          `Failed to fetch all invitations due to: ${error.message}`
        );
      }

      return data.map((row) => this.fromTableRow(row));
    } catch (error) {
      this.log(`Failed to fetch all invitations due to:`, error);
    }

    return [];
  }

  async fetchInvitationByUUID(uuid: string): Promise<Invitation | null> {
    try {
      const { data, error } = await this.supbase
        .from(this.supabaseTable)
        .select('*')
        .eq('uuid', uuid);

      this.log(`[fetchInvitationByUUID] invitation ${uuid} from DB:`, {
        data,
        error,
      });

      if (error) {
        throw new Error(
          `Failed to fetch invitation ${uuid} due to: ${error.message}`
        );
      }

      if (!data || !data.length) {
        throw new Error(`Invitation ${uuid} not found in the database!`);
      }

      return this.fromTableRow(data[0]);
    } catch (error) {
      this.log(error);
    }

    return null;
  }

  async acceptInvitation(invitation: Invitation): Promise<boolean> {
    try {
      const accepted = invitation.acceptedCount ?? 0;
      if (accepted < 0 || accepted > invitation.invitedCount) {
        throw new Error(
          `Invalid accepted count ${accepted} for invitation ${invitation.uuid}`
        );
      }

      const { error } = await this.supbase
        .from(this.supabaseTable)
        .update({ acceptedCount: accepted })
        .eq('uuid', invitation.uuid);

      if (error) {
        throw new Error(
          `Failed to accept invitation ${invitation.uuid} due to: ${error.message}`
        );
      }

      this.log(`Accepted invitation ${invitation.uuid} with count ${accepted}`);

      return true;
    } catch (error) {
      this.log(error);
    }

    return false;
  }

  async declineInvitation(invitation: Invitation): Promise<boolean> {
    try {
      const { error } = await this.supbase
        .from(this.supabaseTable)
        .update({ acceptedCount: 0 })
        .eq('uuid', invitation.uuid);

      if (error) {
        throw new Error(
          `Failed to decline invitation ${invitation.uuid} due to: ${error.message}`
        );
      }

      this.log(`Declined invitation ${invitation.uuid}`);

      return true;
    } catch (error) {
      this.log(error);
    }

    return false;
  }

  async viewInvitation(invitation: Invitation): Promise<boolean> {
    try {
      const { error } = await this.supbase
        .from(this.supabaseTable)
        .update({ viewsCount: invitation.viewsCount })
        .eq('uuid', invitation.uuid);

      if (error) {
        throw new Error(
          `Failed to view invitation ${invitation.uuid} due to: ${error.message}`
        );
      }

      this.log(
        `Viewed invitation ${invitation.uuid} with count ${invitation.viewsCount}`
      );

      return true;
    } catch (error) {
      this.log(error);
    }

    return false;
  }

  async sendInvitation(invitation: Invitation): Promise<boolean> {
    try {
      if (this.dryRun) {
        this.log(
          '[DRY RUN] Skipping sending invitation',
          JSON.stringify(invitation)
        );

        return true;
      }

      // Exit gracefully if there is nothing to save
      if (!invitation.sentAt && !invitation.sid) return true;

      const update = {} as Record<string, unknown>;

      if (invitation.sentAt) {
        update['sentAt'] = invitation.sentAt;
      }

      if (invitation.sid) {
        update['sid'] = invitation.sid;
      }

      const { error } = await this.supbase
        .from(this.supabaseTable)
        .update(update)
        .eq('uuid', invitation.uuid);

      if (error) {
        throw new Error(
          `Failed to send invitation ${invitation.uuid} due to: ${error.message}`
        );
      }

      this.log(`Sent invitation ${invitation.uuid}: ${JSON.stringify(update)}`);

      return true;
    } catch (error) {
      this.log(error);
    }

    return false;
  }

  async updateInvitationErrors(invitation: Invitation): Promise<boolean> {
    try {
      if (this.dryRun) {
        this.log(
          '[DRY RUN] Skipping updating invitation errors',
          JSON.stringify(invitation)
        );

        return true;
      }

      const { error } = await this.supbase
        .from(this.supabaseTable)
        .update({
          errors: invitation.errors ? invitation.errors : null,
        })
        .eq('uuid', invitation.uuid);

      if (error) {
        throw new Error(
          `Failed to update invitation errors for ${invitation.uuid} due to: ${error.message}`
        );
      }

      this.log(
        `Updated invitation errors for ${invitation.uuid}: ${invitation.errors}`
      );

      return true;
    } catch (error) {
      this.log(error);
    }

    return false;
  }

  /**
   * Update Supabase with the latest data from the spreadsheet
   *
   * Only the following fields are updated:
   * - uuid
   * - guest (name, email, phone, locale, isTester)
   * - groomName
   * - brideName
   * - msg
   * - link
   * - invitedCount
   */
  async updateSupabase(): Promise<boolean> {
    try {
      const res = await this.sheets.spreadsheets.values.get({
        spreadsheetId: this.spreadsheetId,
        range: this.sheetRange,
      });

      if (!res.data.values?.length) {
        throw new Error('No rows found in the spreadsheet!');
      }

      const invitations = res.data.values
        .map((row, idx) => this.fromSpreadsheetRow(row, idx))
        .filter((invitation) => this.isValidInvitation(invitation));

      if (!invitations.length) {
        throw new Error('No valid invitations found in the spreadsheet!');
      }

      const { error } = await this.supbase
        .from(this.supabaseTable)
        .upsert(
          invitations.map((invitation) => this.toTableRow(invitation)),
          {
            onConflict: 'uuid',
            ignoreDuplicates: false,
          }
        )
        .select();

      if (error) {
        this.log(error.code, error.message);

        throw new Error(
          `Failed to update Supabase with ${invitations.length} invitations due to: ${error.message}`
        );
      }

      return true;
    } catch (error) {
      this.log(error);
    }

    return false;
  }

  /**
   * Update the spreadsheet with the latest data from Supabase
   *
   * Only the following fields are updated:
   *
   * - acceptedCount
   * - viewsCount
   * - sentAt
   * - sid
   * - errors
   */
  async updateSpreadsheet(): Promise<boolean> {
    try {
      const res = await this.sheets.spreadsheets.values.get({
        spreadsheetId: this.spreadsheetId,
        range: this.sheetRange,
      });

      if (!res.data.values?.length) {
        throw new Error('No rows found in the spreadsheet!');
      }

      const sheetInvitations = res.data.values
        .map((row, idx) => this.fromSpreadsheetRow(row, idx))
        .filter((invitation) => this.isValidInvitation(invitation));

      if (!sheetInvitations.length) {
        throw new Error('No valid invitations found in the spreadsheet!');
      }

      const supabaseInvitations = await this.fetchAllInvitations();

      if (!supabaseInvitations.length) {
        throw new Error('No invitations found in Supabase!');
      }

      const data: sheets_v4.Schema$ValueRange[] = supabaseInvitations
        .map((supabaseInvitation) => {
          const sheetInvitation = sheetInvitations.find(
            (inv) => inv.uuid === supabaseInvitation.uuid
          );

          if (!sheetInvitation) {
            return [];
          }

          return this.toSpreadsheetData({
            ...sheetInvitation,
            ...supabaseInvitation,
          });
        })
        .flat();

      await this.sheets.spreadsheets.values.batchUpdate({
        spreadsheetId: this.spreadsheetId,
        requestBody: {
          data,
          valueInputOption: 'USER_ENTERED',
        },
      });

      return true;
    } catch (error) {
      this.log(`Failed to update spreadsheet due to:`, error);
    }

    return false;
  }

  protected isValidInvitation(invitation: Invitation): boolean {
    const uuid = invitation.uuid;

    return !!uuid && UUID_REGEX.test(uuid);
  }

  protected cellA1Notation(column: DbCol, row: number): string {
    // Add 2 to row to account for header row and zero-based index
    return `${DB_A1NOTATION[column]}${row + 2}`;
  }

  protected fromSpreadsheetRow(row: string[], idx: number): Invitation {
    const uuid = row[DB_UUID_COLUMN];
    const invitedCount = row[DB_INVITED_COUNT_COLUMN]
      ? parseInt(row[DB_INVITED_COUNT_COLUMN])
      : 0;
    const acceptedCount = row[DB_ACCEPTED_COUNT_COLUMN]
      ? parseInt(row[DB_ACCEPTED_COUNT_COLUMN])
      : undefined;
    const status =
      acceptedCount === 0
        ? InvitationStatus.Declined
        : (acceptedCount as number) > 0
        ? InvitationStatus.Accepted
        : InvitationStatus.Pending;
    const msg = row[DB_MSG_COLUMN];
    const name = row[DB_NAME_COLUMN];
    const email = row[DB_EMAIL_COLUMN];
    const phone = row[DB_PHONE_COLUMN];
    const link = row[DB_LINK_COLUMN];
    const isTester = (row[DB_IS_TESTER_COLUMN] ?? '').toLowerCase() === 'yes';
    const viewsCount = row[DB_VIEWS_COUNT_COLUMN]
      ? parseInt(row[DB_VIEWS_COUNT_COLUMN])
      : 0;
    const groomName = row[DB_GROOM_NAME_COLUMN];
    const brideName = row[DB_BRIDE_NAME_COLUMN];
    const sentAt = row[DB_SENT_AT_COLUMN]
      ? new Date(row[DB_SENT_AT_COLUMN])
      : undefined;
    const sid = row[DB_SID_COLUMN];
    const locale = row[DB_LANGUAGE_COLUMN]
      ? LANG_LOCALE_MAP[row[DB_LANGUAGE_COLUMN]]
      : undefined;
    const errors = row[DB_ERRORS_COLUMN];

    return {
      uuid,
      idx,
      msg,
      invitedCount,
      acceptedCount,
      status,
      link,
      guest: {
        name,
        email,
        phone,
        isTester,
      },
      viewsCount,
      groomName,
      brideName,
      sentAt,
      sid,
      locale,
      errors,
    };
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  protected toTableRow(invitation: Invitation): any {
    return {
      uuid: invitation.uuid,
      invitedCount: invitation.invitedCount,
      guestName: invitation.guest?.name ?? null,
      guestEmail: invitation.guest?.email ?? null,
      guestPhone: invitation.guest?.phone ?? null,
      isTester: invitation.guest?.isTester ?? false,
      locale: invitation.locale ?? 'en',
      groomName: invitation.groomName ?? null,
      brideName: invitation.brideName ?? null,
      msg: invitation.msg ?? null,
      link: invitation.link,
    };
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  protected fromTableRow(row: any): Invitation {
    return {
      uuid: row['uuid'],
      invitedCount: row['invitedCount'],
      guest: {
        name: row['guestName'],
        email: row['guestEmail'],
        phone: row['guestPhone'],
        isTester: row['isTester'],
      },
      locale: row['locale'],
      groomName: row['groomName'],
      brideName: row['brideName'],
      msg: row['msg'],
      link: row['link'],
      acceptedCount: row['acceptedCount'],
      viewsCount: row['viewsCount'],
      sentAt: row['sentAt'] ? new Date(row['sentAt']) : undefined,
      sid: row['sid'],
      errors: row['errors'],
      status:
        row['acceptedCount'] === 0
          ? InvitationStatus.Declined
          : (row['acceptedCount'] as number) > 0
          ? InvitationStatus.Accepted
          : InvitationStatus.Pending,
    } as Invitation;
  }

  protected toSpreadsheetData(
    invitation: Invitation
  ): sheets_v4.Schema$ValueRange[] {
    return [
      {
        range: this.cellA1Notation(DB_ACCEPTED_COUNT_COLUMN, invitation.idx),
        values: [[invitation.acceptedCount?.toString() ?? '']],
      },
      {
        range: this.cellA1Notation(DB_VIEWS_COUNT_COLUMN, invitation.idx),
        values: [[invitation.viewsCount?.toString() ?? '']],
      },
      {
        range: this.cellA1Notation(DB_SENT_AT_COLUMN, invitation.idx),
        values: [[invitation.sentAt?.toISOString() ?? '']],
      },
      {
        range: this.cellA1Notation(DB_SID_COLUMN, invitation.idx),
        values: [[invitation.sid ?? '']],
      },
      {
        range: this.cellA1Notation(DB_ERRORS_COLUMN, invitation.idx),
        values: [[invitation.errors ?? '']],
      },
    ];
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private log(...args: any[]): void {
    console.log('[DatabaseService] ', ...args);
  }
}

export const databaseService = new DatabaseService(
  createSheetsClient(),
  process.env['DB_SPREADSHEET_ID'] as string,
  process.env['DB_SHEET_RANGE'] as string,
  createSupabaseClient(),
  process.env['SUPABASE_TABLE'] as string
);

function createSheetsClient(): sheets_v4.Sheets {
  // Decode the base64-encoded string
  const serviceAccountKey = Buffer.from(
    process.env['GOOGLE_SERVICE_ACCOUNT_KEY'] as string,
    'base64'
  ).toString('utf-8');
  const serviceAccount = JSON.parse(serviceAccountKey);

  const auth = new google.auth.GoogleAuth({
    credentials: serviceAccount,
    scopes: ['https://www.googleapis.com/auth/spreadsheets'],
  });

  return google.sheets({ version: 'v4', auth });
}

function createSupabaseClient(): SupabaseClient {
  return createClient(
    process.env['SUPABASE_URL'] as string,
    process.env['SUPABASE_ANON_KEY'] as string
  );
}
