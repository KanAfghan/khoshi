const { createGlobPatternsForDependencies } = require('@nx/react/tailwind');
const { join } = require('path');
const defaultTheme = require('tailwindcss/defaultTheme')

/** @type {import('tailwindcss').Config} */
module.exports = {
  presets: [require('../../tailwind-workspace-preset.js')],
  content: [
    join(
      __dirname,
      'app/**/*!(*.spec).{ts,tsx,html}'
    ),
    ...createGlobPatternsForDependencies(__dirname),
  ],
  theme: {
    extend: {
      fontFamily: {
        bellefair: ['var(--bellefair)', ...defaultTheme.fontFamily.sans],
        belleza: ['var(--belleza)', ...defaultTheme.fontFamily.sans],
        glyph: ['var(--noto-naskh-arabic)'],
      },
      backgroundImage: {
        'wildflowers': "url('/images/wildflowers-background.png')",
      },
    },
  },
  plugins: [],
};
