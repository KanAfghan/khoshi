import 'server-only';
import type { Locale } from './i18n-config';

const dictionaries = {
  en: () => import('./locales/en.json').then((m) => m.default),
  ps: () => import('./locales/ps.json').then((m) => m.default),
  fa: () => import('./locales/fa.json').then((m) => m.default),
};

export const getDictionary = async (locale: Locale) =>
  dictionaries[locale]?.() ?? dictionaries.en();
