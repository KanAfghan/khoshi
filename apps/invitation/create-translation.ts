export function createTranslation(dictionary: Record<string, string>) {
  return {
    t: (key: string, params?: Record<string, string | number>) => {
      let translation = dictionary[key];
      if (!translation) {
        throw new Error(`Translation for key "${key}" not found`);
      }

      if (params) {
        Object.entries(params).forEach(([key, value]) => {
          translation = translation.replace(`{{${key}}}`, String(value));
        });
      }

      return translation;
    },
  };
}
