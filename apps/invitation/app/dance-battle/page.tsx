'use client';

import MuxPlayer from '@mux/mux-player-react/lazy';

const DANCE_BATTLE_VIDEOS = [
  {
    playbackId: 'GCA00q2PbNoqGvqsXmzRgNgsiU5SixKItFyLR8I4ZAqs',
    title: '1 Las Ketchup - The Ketchup Song',
  },
  {
    playbackId: 'CGztcc9RcVJwDkRc4M2Vp7200ZtW6d1VNdA1P00mBZoo4',
    title: '2 Macarena - Macarena',
  },
  {
    playbackId: 'NWIFRfTjcmZlMcef02s02H02JUNgEtRTIDeC7gG2iq01NrQ',
    title: '3 YMCA - Village People',
  },
  {
    playbackId: 'HnEvwHJ2CgUruqAiwV8arfkb2FVmh5x5nrBdrMJPoy8',
    title: '4 Fireball - Pitbull',
  },
  {
    playbackId: '02Rm1Vpt4ELWx01S8667oThjPck4bULiia1uzPsCWw3C00',
    title: '5 Tere Vaaste - Zara Hatke Zara Bachke',
  },
  {
    playbackId: 'JblgIXcOhmZtZ66UvxnBIDauWvqLhiTv00gTeOHAN8Y4',
    title: '6 Waka Waka - Shakira',
  },
  {
    playbackId: 'I3zxt6RpuJw1Io9b5hVZy02SoP2Gac00sFUYjaFtGPWR00',
    title: '7 Jerusalema - Master KG',
  },
  {
    playbackId: 'M02L7DC1t8thhkjSVwS4uTsaNflUFUBRX00fxVtm01MUIw',
    title: '8 Dancing Queen - ABBA',
  },
  {
    playbackId: 'b9z6h5a6nfclrad3Qa02yqH1oyMaGn00BFhw7lZl00M02oA',
    title: '9 Bolo Tara Ra Ra - Daler Mehndi',
  },
  {
    playbackId: 'ccrkxsY02MYVHSSBs023yJ5ihjS7SJ4FrNxuvCObJ02vT00',
    title: 'Last one - Surprise!',
  },
];

const DanceBattlePage = () => {
  return (
    <div className="flex-1 flex flex-col justify-center items-center p-6">
      <div className="flex flex-col space-y-6">
        <h1 className="text-2xl font-bold">Dance Battle</h1>
        <p>The following songs will be part of the Dance Battle.</p>

        <div className="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
          {DANCE_BATTLE_VIDEOS.map(({ playbackId, title }) => (
            <Video key={playbackId} playbackId={playbackId} title={title} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default DanceBattlePage;

async function Video({
  playbackId,
  title,
}: {
  playbackId: string;
  title: string;
}) {
  return (
    <div className="px-6 max-h-48 md:px-0 md:max-h-72">
      <div className="text-sm mb-1">{title}</div>
      <MuxPlayer
        streamType="on-demand"
        loading="viewport"
        playbackId={playbackId}
        metadataVideoTitle={title}
        primaryColor="#FFFFFF"
        secondaryColor="#000000"
        style={{ width: '100%', height: '80%' }}
      />
    </div>
  );
}
