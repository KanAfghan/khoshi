import { Metadata } from 'next';

export function generateMetadata(): Metadata {
  return {
    title: 'Hawaon Ne Ye Kaha - Dance Battle',
    description:
      'Dance Battle version of "Hawaon Ne Ye Kaha" from the movie "Aap Mujhe Achche Lagne Lage".',
  };
}

export default async function Layout({
  children,
}: {
  children: React.ReactNode;
}) {
  return <>{children}</>;
}
