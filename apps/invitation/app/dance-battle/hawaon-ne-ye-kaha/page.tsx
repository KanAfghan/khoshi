'use client';

import MuxPlayer from '@mux/mux-player-react/lazy';

const PLAYBACK_ID = '3RHHmk7GZLLFzRXQfg8G9Pl5PyOxFaO401VhD9Z2jYVQ';

const HawaonNeYehKahaPage = () => {
  return (
    <div className="flex-1 flex flex-col justify-center items-center p-6">
      <div className="flex flex-col space-y-6">
        <h1 className="text-2xl font-bold">Hawaon Ne Ye Kaha</h1>

        <div className="sm:max-w-sm">
          <MuxPlayer
            streamType="on-demand"
            loading="viewport"
            playbackId={PLAYBACK_ID}
            metadataVideoTitle="Hawaon Ne Ye Kaha"
            primaryColor="#FFFFFF"
            secondaryColor="#000000"
            style={{ width: '100%', height: '80%' }}
          />
        </div>
      </div>
    </div>
  );
};

export default HawaonNeYehKahaPage;
