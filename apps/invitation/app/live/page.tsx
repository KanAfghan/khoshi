'use client';

import MuxPlayer from '@mux/mux-player-react/lazy';

const LivePage = () => {
  return (
    <div className="flex-1 flex flex-col justify-center items-center p-6">
      <div className="flex flex-col space-y-6">
        <div className="sm:max-w-sm">
          <MuxPlayer
            streamType="live"
            playbackId="szIIbwNVxy02zbwYEwptZwNbS93DZZKtXwKJQboOJ01ro"
            metadataVideoTitle="Stream 1"
            primaryColor="#FFFFFF"
            secondaryColor="#000000"
            style={{ width: '100%', height: '80%' }}
          />
        </div>
      </div>
    </div>
  );
};

export default LivePage;
