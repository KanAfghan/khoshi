import { NextRequest, NextResponse } from 'next/server';
import { databaseService } from 'data-access';

export async function GET(request: NextRequest) {
  // Authorization header should match `SYNC_AUTH_TOKEN` env variable
  const token = request.headers.get('Authorization');
  if ((token || '').replace('Bearer ', '') !== process.env.SYNC_AUTH_TOKEN) {
    log('Invalid token:', token);

    return NextResponse.json(
      {
        message: 'Invalid token!',
      },
      {
        status: 401,
      }
    );
  }

  log('Starting sync job');

  let message = 'Sync completed successfully!';
  let status = 200;

  try {
    await databaseService.updateSupabase();

    await databaseService.updateSpreadsheet();

    log('Sync completed successfully!');
  } catch (error) {
    log('Failed to sync:', error);

    message = 'Failed to sync!';
    status = 500;
  }

  return NextResponse.json(
    {
      message,
    },
    {
      status,
    }
  );
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function log(...args: any[]) {
  console.log('[SYNC] ', ...args);
}
