'use server';

import { databaseService } from 'data-access';
import { revalidatePath } from 'next/cache';

export async function acceptInvitation(uuid: string, formData: FormData) {
  console.log('[Server Action] accepting invitation...', uuid);
  if (!formData.get('count')) {
    throw Error(`Missing "count"!`);
  }

  const acceptedCount = Number(formData.get('count'));
  if (acceptedCount < 0) {
    throw Error(`Accepted count "${acceptedCount}" is less than zero!`);
  }

  const invitation = await databaseService.fetchInvitationByUUID(uuid);
  if (!invitation) {
    throw Error(`Invitation "${uuid}" not found!`);
  }

  const { invitedCount } = invitation;
  if (acceptedCount > invitedCount) {
    throw Error(
      `Accepted count "${acceptedCount}" is greater than invited count "${invitedCount}"!`
    );
  }

  invitation.acceptedCount = acceptedCount;
  const result = await databaseService.acceptInvitation(invitation);
  if (!result) {
    throw Error(`Failed to accept invitation "${uuid}"!`);
  }
  console.log(`[Server Action] invitation "${uuid}" accepted!`, invitation);

  revalidatePath(`/${uuid}`);
}

export async function declineInvitation(uuid: string) {
  console.log('[Server Action] declining invitation...', uuid);

  const invitation = await databaseService.fetchInvitationByUUID(uuid);
  if (!invitation) {
    throw Error(`Invitation "${uuid}" not found!`);
  }

  const result = await databaseService.declineInvitation(invitation);
  if (!result) {
    throw Error(`Failed to decline invitation "${uuid}"!`);
  }
  console.log(`[Server Action] invitation "${uuid}" declined!`, invitation);

  revalidatePath(`/${uuid}`);
}
