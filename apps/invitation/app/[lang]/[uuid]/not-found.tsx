export default function NotFound() {
  return (
    <div className="flex-1 flex flex-col justify-center items-center">
      <div className="bg-red-vivid-200 bg-opacity-25 m-6 p-6 rounded shadow">
        <h1 className="text-red-vivid-800 font-bold text-xl">Not Found</h1>
        <p className="text-red-vivid-800">
          Sorry, we couldn&apos;t find what you were looking for.
        </p>
      </div>
    </div>
  );
}
