import { notFound } from 'next/navigation';
import { Invitation, InvitationStatus } from 'core';
import { databaseService } from 'data-access';
import { RegisterView } from './register-view';
import { InvitationCard, InvitationResponseCard } from 'shared-ui';
import { acceptInvitation, declineInvitation } from './actions';
import { i18n, type Locale } from '../../../i18n-config';
import { getDictionary } from '../../../get-dictionary';
import { createTranslation } from '../../../create-translation';

type Props = {
  params: { lang: Locale; uuid: string };
};

export default async function Page({ params }: Props) {
  if (!i18n.locales.includes(params.lang)) {
    notFound();
  }

  const dictionary = await getDictionary(params.lang);
  const { t } = createTranslation(dictionary);

  const invitation: Invitation | null =
    await databaseService.fetchInvitationByUUID(params.uuid);

  if (!invitation) {
    notFound();
  }

  const { msg } = invitation;

  const groomNameEnvVar =
    params.lang !== 'en'
      ? `NEXT_PUBLIC_GROOM_NAME_${params.lang.toUpperCase()}`
      : 'NEXT_PUBLIC_GROOM_NAME';
  const brideNameEnvVar =
    params.lang !== 'en'
      ? `NEXT_PUBLIC_BRIDE_NAME_${params.lang.toUpperCase()}`
      : 'NEXT_PUBLIC_BRIDE_NAME';

  const groomName = {
    native: process.env[groomNameEnvVar] as string,
    latin: process.env.NEXT_PUBLIC_GROOM_NAME as string,
  };
  const brideName = {
    native: process.env[brideNameEnvVar] as string,
    latin: process.env.NEXT_PUBLIC_BRIDE_NAME as string,
  };

  return (
    <div className="flex-1 flex flex-col space-y-8 mx-6 mt-8 lg:max-w-4xl lg:container lg:py-6 lg:m-auto">
      <InvitationCard
        imageUrl={process.env.NEXT_PUBLIC_HERO_IMAGE_URL as string}
        groomName={groomName}
        brideName={brideName}
        msg={msg}
        pendingResponse={invitation.status === InvitationStatus.Pending}
        accepted={invitation.status === InvitationStatus.Accepted}
        t={t}
        locale={params.lang}
      />

      <InvitationResponseCard
        invitation={invitation}
        acceptInvitation={acceptInvitation}
        declineInvitation={declineInvitation}
        t={t}
        locale={params.lang}
      />

      <RegisterView
        uuid={params.uuid}
        label={t('page:invitationId')}
        lang={params.lang as string}
      />
    </div>
  );
}
