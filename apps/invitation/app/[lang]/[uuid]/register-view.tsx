'use client';

import { useEffect } from 'react';
import { useInView } from 'react-intersection-observer';

export interface RegisterViewProps {
  uuid: string;
  label: string;
  lang: string;
}

export function RegisterView({ uuid, label, lang }: RegisterViewProps) {
  const { ref, inView } = useInView({ triggerOnce: true });

  useEffect(() => {
    if (inView) {
      fetch(`/${lang}/${uuid}/views`, {
        method: 'POST',
      })
        .then((res) => {
          console.log('Registered view', res.statusText);

          if (!res.ok) {
            console.error('Failed to register view');

            return;
          }
        })
        .catch((err) => {
          console.error('Failed to register view', err);
        });
    }
  }, [inView, uuid, lang]);

  return (
    <div
      ref={ref}
      className="p-2 text-center text-cool-grey-300 text-xs rtl:text-sm"
    >
      {label} <span className="rtl:text-xs">{uuid}</span>
    </div>
  );
}
