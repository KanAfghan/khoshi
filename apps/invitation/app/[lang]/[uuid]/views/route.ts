import { NextRequest, NextResponse } from 'next/server';
import { databaseService } from 'data-access';

interface Context {
  params: {
    uuid: string;
  };
}

export async function POST(
  req: NextRequest,
  { params }: Context
): Promise<NextResponse> {
  if (!params.uuid) {
    return new NextResponse('', {
      status: 400,
    });
  }

  const invitation = await databaseService.fetchInvitationByUUID(params.uuid);
  if (!invitation) {
    return new NextResponse('', {
      status: 404,
    });
  }

  invitation.viewsCount = invitation.viewsCount ? invitation.viewsCount + 1 : 1;
  const result = await databaseService.viewInvitation(invitation);
  if (!result) {
    return new NextResponse('', {
      status: 500,
    });
  }

  return new NextResponse('', {
    status: 200,
  });
}
