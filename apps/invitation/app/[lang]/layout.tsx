import { Bellefair, Noto_Naskh_Arabic, Belleza } from 'next/font/google';
import { Analytics } from '@vercel/analytics/react';
import { i18n, type Locale } from '../../i18n-config';
import { getDictionary } from '../../get-dictionary';
import { createTranslation } from '../../create-translation';
import { Metadata } from 'next';

const belleza = Belleza({
  subsets: ['latin', 'latin-ext'],
  weight: '400',
  display: 'swap',
  variable: '--belleza',
});

const bellefair = Bellefair({
  subsets: ['latin', 'latin-ext'],
  weight: '400',
  display: 'swap',
  variable: '--bellefair',
});

const noto_naskh_arabic = Noto_Naskh_Arabic({
  subsets: ['arabic'],
  weight: ['400', '500', '600', '700'],
  display: 'swap',
  variable: '--noto-naskh-arabic',
});

type Props = {
  params: { lang: Locale };
};

export async function generateStaticParams() {
  return i18n.locales.map((locale) => ({ lang: locale }));
}

export async function generateMetadata({
  params: { lang },
}: Props): Promise<Metadata> {
  const dictionary = await getDictionary(lang);
  const { t } = createTranslation(dictionary);

  const groomNameEnvVar =
    lang !== 'en'
      ? `NEXT_PUBLIC_GROOM_NAME_${lang.toUpperCase()}`
      : 'NEXT_PUBLIC_GROOM_NAME';
  const brideNameEnvVar =
    lang !== 'en'
      ? `NEXT_PUBLIC_BRIDE_NAME_${lang.toUpperCase()}`
      : 'NEXT_PUBLIC_BRIDE_NAME';

  return {
    title: t('page:title', {
      groom_name: process.env[groomNameEnvVar] as string,
      bride_name: process.env[brideNameEnvVar] as string,
    }),
    description: t('page:description', {
      groom_name: process.env[groomNameEnvVar] as string,
      bride_name: process.env[brideNameEnvVar] as string,
    }),
  };
}

export default async function RootLayout({
  children,
  params,
}: {
  children: React.ReactNode;
} & Props) {
  const dictionary = await getDictionary(params.lang);
  const { t } = createTranslation(dictionary);

  const copyRightYear = new Date().getFullYear();
  const copyRightHolder = process.env.NEXT_PUBLIC_COPYRIGHT_HOLDER;

  return (
    <html
      lang={params.lang}
      dir={params.lang === 'en' ? 'ltr' : 'rtl'}
      className={`${bellefair.variable} ${noto_naskh_arabic.variable} ${belleza.variable}`}
    >
      <body className="bg-wildflowers bg-contain bg-no-repeat">
        <div className="min-h-screen flex flex-col">
          <main className="relative flex-1 text-grey-500 text-base flex flex-col">
            {children}
          </main>

          <footer className="flex justify-center items-center bg-grey-100 bg-opacity-50 text-grey-700 py-4 px-6 text-center">
            &copy;{' '}
            {t('page:copyright', {
              year: copyRightYear,
              owner: copyRightHolder as string,
            })}
          </footer>
        </div>

        {!!process.env.NEXT_PUBLIC_ENABLE_ANALYTICS && <Analytics />}
      </body>
    </html>
  );
}
