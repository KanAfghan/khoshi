import { Analytics } from '@vercel/analytics/react';

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const copyRightYear = new Date().getFullYear();
  const copyRightHolder = process.env.NEXT_PUBLIC_COPYRIGHT_HOLDER;

  return (
    <html lang="en">
      <body className="bg-wildflowers bg-contain bg-no-repeat">
        <div className="min-h-screen flex flex-col">
          <main className="relative flex-1 text-grey-500 text-base flex flex-col">
            {children}
          </main>

          <footer className="flex justify-center items-center bg-grey-100 bg-opacity-50 text-grey-700 py-4 px-6 text-center">
            &copy; Copyright {copyRightYear} {copyRightHolder}, All rights
            reserved.
          </footer>
        </div>

        {!!process.env.NEXT_PUBLIC_ENABLE_ANALYTICS && <Analytics />}
      </body>
    </html>
  );
}
