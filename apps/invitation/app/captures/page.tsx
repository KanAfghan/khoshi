'use client';

import { CldUploadButton } from 'next-cloudinary';

const CapturesPage = () => {
  return (
    <div className="flex-1 flex flex-col justify-center items-center p-6">
      <div className="flex flex-col space-y-8">
        <h1 className="text-2xl font-bold">Upload Your Captures</h1>
        <p>
          Upload up to 10 of your captures (photos and videos) and share them
          with us.
        </p>

        <CldUploadButton
          signatureEndpoint="/captures/sign-uploads"
          options={{
            sources: ['local'],
            multiple: true,
            maxFiles: 10,
            clientAllowedFormats: ['image', 'video'],
          }}
          className="px-6 py-1.5 rounded-md font-semibold shadow-md border-grey-300 border text-lime-green-900"
        >
          Start uploading
        </CldUploadButton>
      </div>
    </div>
  );
};

export default CapturesPage;
