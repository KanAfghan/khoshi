import { notFound, redirect } from 'next/navigation';
import { urlShortnerService } from 'data-access';

type Props = {
  params: { hash: string };
};

export default async function Page({ params }: Props) {
  if (!params.hash) {
    notFound();
  }

  const url = await urlShortnerService.expand(params.hash);
  if (!url) {
    notFound();
  }

  redirect(url);
}
