import { NextRequest, NextResponse } from 'next/server';
import { databaseService } from 'data-access';
import { Invitation } from 'core';

export async function GET(request: NextRequest) {
  // Authorization header should match `CRON_AUTH_TOKEN` env variable
  const token = request.headers.get('Authorization');
  if ((token || '').replace('Bearer ', '') !== process.env.CRON_AUTH_TOKEN) {
    log('Invalid token:', token);

    return NextResponse.json(
      {
        message: 'Invalid token!',
      },
      {
        status: 401,
      }
    );
  }

  log('Starting job for all invitations');

  let message = 'Cron job completed successfully!';
  let status = 200;

  try {
    const invitations = await databaseService.fetchAllInvitations();
    if (!invitations) {
      log('Failed to fetch invitations!');

      message = 'Found no invitations!';
      status = 500;
      return NextResponse.json(
        {
          message,
        },
        {
          status,
        }
      );
    }

    log('Total Invitations to Process:', invitations.length);

    await Promise.all(
      invitations.map((i) => processInvitation(i, request.url))
    );

    log('Cron job completed successfully!');
  } catch (error) {
    console.log('Failed to process invitations due to:', error);

    message = 'Failed to process invitations!';
    status = 500;
  }

  return NextResponse.json(
    {
      message,
    },
    {
      status,
    }
  );
}

async function processInvitation(
  invitation: Invitation,
  baseUrl: string
): Promise<void> {
  log('[processInvitation()] Processing invitation:', invitation.uuid, baseUrl);

  try {
    const url = baseUrl + '/' + invitation.uuid;
    log('[processInvitation()] URL:', url);

    const result = await fetch(url, {
      headers: {
        Authorization: `Bearer ${process.env.CRON_AUTH_TOKEN}`,
      },
      cache: 'no-store',
    });

    if (!result.ok)
      throw new Error(
        `Failed to process invitation due to: "${result.statusText}"`
      );
  } catch (error) {
    log('[processInvitation()] [ERROR]', error);
  }

  log(
    `[processInvitation()] Invitation "${invitation.uuid}" processed successfully!`
  );
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function log(...args: any[]) {
  console.log('[CRON] ', ...args);
}
