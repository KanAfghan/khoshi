import { NextRequest, NextResponse } from 'next/server';
import {
  NotificationStrategy,
  databaseService,
  notificationsService,
} from 'data-access';
import { Invitation } from 'core';

interface Params {
  params: { uuid: string };
}

export async function GET(request: NextRequest, { params }: Params) {
  // Authorization header should match `CRON_AUTH_TOKEN` env variable
  const token = request.headers.get('Authorization');
  if ((token || '').replace('Bearer ', '') !== process.env.CRON_AUTH_TOKEN) {
    log('Invalid token:', token);

    return NextResponse.json(
      {
        message: 'Invalid token!',
      },
      {
        status: 401,
      }
    );
  }

  log('Starting job for UUID:', params.uuid);

  const dryRun = !!process.env.CRON_DRY_RUN;

  log('Starting job with dry run:', dryRun);

  notificationsService.isDryRun = dryRun;

  log('Setting notification strategy to:', NotificationStrategy.WhatsApp);
  notificationsService.strategy = NotificationStrategy.WhatsApp;

  databaseService.isDryRun = dryRun;

  let message = 'Job completed successfully!';
  let status = 200;

  try {
    const invitation = await databaseService.fetchInvitationByUUID(params.uuid);
    if (!invitation) {
      log('Invitation not found!');

      message = 'Not found!';
      status = 404;
      return NextResponse.json(
        {
          message,
        },
        {
          status,
        }
      );
    }

    const tasks: Promise<boolean>[] = [];

    const [validatedInvite, invalid] = validateInvitation(invitation);

    if (invalid) {
      tasks.push(processInvalidInvitation(validatedInvite));
    } else {
      tasks.push(processNewInvitation(invitation));
      tasks.push(processSentInvitation(invitation));
      tasks.push(processFailedInvitation(invitation));
      tasks.push(processUndeliveredInvitation(invitation));
    }

    await Promise.all(tasks);

    log(`Job for UUID: ${params.uuid} completed successfully!`);
  } catch (error) {
    console.log(`Failed to process invitation "${params.uuid}" due to:`, error);

    message = 'Failed to process invitation!';
    status = 500;
  }

  return NextResponse.json(
    {
      message,
    },
    {
      status,
    }
  );
}

async function processInvalidInvitation(
  invitation: Invitation
): Promise<boolean> {
  log(
    '[processInvalidInvitation()] Processing invalid invitation: ',
    JSON.stringify(invitation)
  );

  try {
    return databaseService.updateInvitationErrors(invitation);
  } catch (error) {
    log(
      '[processInvalidInvitation()] [ERROR] Failed to process invalid invitation:',
      invitation.uuid,
      'due to:',
      error
    );
  }

  return false;
}

async function processNewInvitation(invitation: Invitation): Promise<boolean> {
  try {
    // Skip processing for guests without a phone number
    if (!invitation.guest?.phone?.length) return true;

    if (invitation.sentAt || invitation.sid || invitation.errors) {
      log(
        '[processNewInvitation()] Invitation has already been processed:',
        invitation.uuid
      );

      return true;
    }

    log(
      '[processNewInvitation()] Processing new invitation: ',
      JSON.stringify(invitation)
    );

    // Check if the invitation has already been sent
    const didSendNotification =
      await notificationsService.didSendInvitationNotification(invitation);
    if (didSendNotification && didSendNotification.sid) {
      log(
        '[processNewInvitation()] Invitation has already been sent:',
        invitation.uuid
      );

      return databaseService.sendInvitation(invitation);
    }

    const sentInvitation =
      await notificationsService.sendInvitationNotification(invitation);
    if (!sentInvitation) {
      log(
        '[processNewInvitation()] [ERROR] Failed to send message to: ',
        invitation.uuid
      );

      invitation.errors = 'Failed to send message!';

      return databaseService.updateInvitationErrors(invitation);
    }

    return Promise.all([
      databaseService.sendInvitation(sentInvitation as Invitation),
      databaseService.updateInvitationErrors(sentInvitation as Invitation),
    ]).then((res) => res.every(Boolean));
  } catch (error) {
    log(
      '[processNewInvitation()] [ERROR] Failed to process new invitation:',
      invitation.uuid,
      'due to:',
      error
    );
  }

  return false;
}

async function processSentInvitation(invitation: Invitation): Promise<boolean> {
  try {
    if (invitation.errors || invitation.sentAt) {
      log(
        '[processSentInvitation()] Invitation has already been processed:',
        invitation.uuid
      );

      return true;
    }

    if (!invitation.sid) {
      throw new Error(`Invitation ${invitation.uuid} has not been sent yet!`);
    }

    log(
      '[processSentInvitation()] Processing sent invitation: ',
      JSON.stringify(invitation)
    );

    const sentAt = await notificationsService.fetchSentAt(
      invitation.guest?.phone ?? ''
    );
    if (!sentAt) {
      log(
        '[processSentInvitation()] [ERROR] Failed to fetch sentAt for: ',
        invitation.uuid
      );

      return databaseService.updateInvitationErrors({
        ...invitation,
        errors: 'Failed to fetch sentAt!',
      });
    }

    return databaseService.sendInvitation({
      ...invitation,
      sentAt,
    });
  } catch (error) {
    log(
      '[processSentInvitation()] [ERROR] Failed to process sent invitation:',
      invitation.uuid,
      'due to:',
      error
    );
  }

  return false;
}

async function processFailedInvitation(
  invitation: Invitation
): Promise<boolean> {
  try {
    if (
      !invitation.errors ||
      invitation.errors !== 'Failed to fetch sentAt!' ||
      !invitation.sid
    ) {
      return true;
    }

    log(
      '[processFailedInvitation()] Processing failed invitation: ',
      JSON.stringify(invitation)
    );

    const reason = await notificationsService.fetchFailureReason(invitation);
    if (!reason) {
      log(
        '[processFailedInvitation()] [ERROR] Failed to fetch failure reason for: ',
        invitation.uuid
      );

      invitation.errors =
        invitation.errors + ';' + 'Failed to fetch failure reason!';

      return databaseService.updateInvitationErrors(invitation);
    }

    return databaseService.updateInvitationErrors({
      ...invitation,
      errors: reason + ' | ' + invitation.errors,
    });
  } catch (error) {
    log(
      '[processFailedInvitation()] [ERROR] Failed to check failure reason for invitation:',
      invitation.uuid,
      'due to:',
      error
    );
  }

  return false;
}

async function processUndeliveredInvitation(
  invitation: Invitation
): Promise<boolean> {
  try {
    if (
      !invitation.errors ||
      !['Failed to send SMS', 'Failed to send message!'].some(
        (err) => err === invitation.errors
      )
    ) {
      log(
        '[processUndeliveredInvitation()] Invitation has already been processed:',
        invitation.uuid
      );

      return true;
    }

    log(
      '[processUndeliveredInvitation()] Processing undelivered invitation: ',
      JSON.stringify(invitation)
    );

    notificationsService.strategy = NotificationStrategy.WhatsApp;

    const i = { ...invitation };
    delete i.errors;

    const sentInvitation =
      await notificationsService.sendInvitationNotification(i);
    if (!sentInvitation) {
      log(
        '[processUndeliveredInvitation()] [ERROR] Failed to send WhatsApp message to: ',
        invitation.uuid
      );

      return databaseService.updateInvitationErrors({
        ...i,
        errors: 'Error sending WhatsApp message!' + ';' + invitation.errors,
      });
    }

    return Promise.all([
      databaseService.sendInvitation(sentInvitation),
      databaseService.updateInvitationErrors(sentInvitation),
    ]).then((res) => res.every(Boolean));
  } catch (error) {
    log(
      '[processUndeliveredInvitation()] [ERROR] Failed to check failure reason for invitation:',
      invitation.uuid,
      'due to:',
      error
    );
  }

  return false;
}

function validateInvitation(invitation: Invitation): [Invitation, boolean] {
  // Copy invitation and reset errors field
  const i = { ...invitation };
  delete i.errors;

  if (!hasValidNativeGuestName(i)) {
    i.errors = 'Invalid guest name! Spell the name in the correct language!';

    return [i, true];
  }

  if (!hasValidLatinGuestName(i)) {
    i.errors =
      'Invalid guest name! Spell the name in English and avoid using "+", use "&" instead!';

    return [i, true];
  }

  return [invitation, false];
}

function hasValidLatinGuestName(invitation: Invitation): boolean {
  const { guest, locale } = invitation;

  // Skip validation for guests without a phone number and for non-English locales
  if (!guest?.phone || locale !== 'en') return true;

  return /^[a-z, &]+$/i.test(guest?.name ?? '');
}

function hasValidNativeGuestName(invitation: Invitation): boolean {
  const { guest, locale } = invitation;

  // Skip validation for guests without a phone number and for English locale
  if (!guest?.phone || locale === 'en') return true;

  return /^[^0-1a-z]+$/i.test(guest?.name ?? '');
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function log(...args: any[]) {
  console.log('[CRON] ', ...args);
}
